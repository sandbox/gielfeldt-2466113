<?php
/**
 * @file
 * Interfaces and bases for deferred cache backends.
 */

/**
 * Interfaces.
 */

/**
 * Deferred cache interface.
 */
interface DrupalCacheDeferredInterface {
  /**
   * Returns data from the persistent cache when given an array of cache IDs.
   *
   * @param array $cids
   *   An array of cache IDs for the data to retrieve.
   *
   * @return DrupalCacheDeferredResultInterface
   *   A result object of the cache query.
   */
  public function getMultipleDeferred($cids);
}

/**
 * Deferred cache result interface.
 */
interface DrupalCacheDeferredResultInterface {
  /**
   * Constructor for a result interface.
   *
   * @param DrupalCacheDeferredInterface $cache
   *   The Drupal cache object that was used to retrieve this result.
   * @param mixed $query
   *   The query executed.
   * @param array $cids
   *   The cache IDs attempted retrieved.
   */
  public function __construct($cache, $query, $cids);

  /**
   * Return the result of the query.
   *
   * @return array
   *   An array of the items successfully returned from cache indexed by cid.
   */
  public function getResult();

  /**
   * Get the cache IDs that were not retrieved by the query.
   *
   * @return array
   *   An array of cache IDs
   */
  public function getCidsMissed();

  /**
   * Retrieve the actual data of the cache query.
   *
   * @return array
   *   Cache items indexed by cache ID.
   */
  public function fetch();
}

/**
 * Base classes.
 */

/**
 * Base class for deferred cache results.
 */
class DrupalCacheDeferredResult implements DrupalCacheDeferredResultInterface {
  protected $cache;
  protected $query;
  protected $cids;
  protected $resultFetched = FALSE;
  protected $result;
  protected $cidsMissed = array();

  /**
   * Implements DrupalCacheDeferredInterface::__construct().
   */
  public function __construct($cache, $query, $cids) {
    $this->cache = $cache;
    $this->query = $query;
    $this->cids = $cids;
  }

  /**
   * Implements DrupalCacheDeferredInterface::getResult().
   */
  public function getResult() {
    if (!$this->resultFetched) {
      $this->result = $this->fetch();
      $this->resultFetched = TRUE;
      $this->cidsMissed = array_diff($this->cids, array_keys($this->result));
    }
    return $this->result;
  }

  /**
   * Implements DrupalCacheDeferredInterface::getCidsMissed().
   */
  public function getCidsMissed() {
    $this->getResult();
    return $this->cidsMissed;
  }

  /**
   * Implements DrupalCacheDeferredInterface::fetch().
   */
  public function fetch() {
    return $this->query;
  }
}
